---
marp: true
math: mathjax 
theme: uncover
color: #0F1F2E 
backgroundColor: #F4F8FF
style: |
    h1 {
        font-family: 'Poppins';
        --color-highlight-heading: #0F7CFB

    }
    h2 {
        font-family: 'Poppins';
        --color-highlight-heading: #0F7CFB

    }
    h3 {
        font-family: 'Poppins';
        --color-highlight-heading: #0F7CFB

    }
    h4 {
        font-family: 'Poppins';
        --color-highlight-heading: #0F7CFB

    }
    section {
        font-family: 'Manrope';
        --color-highlight: #0f7cfb;
        --color-highlight-hover: #00dfff;  
    }
    code {
        font-family: 'Fira Code';
    }
    header {
        font-family: 'Manrope';
    }
    footer {
        font-family: 'Manrope';
    }
    .columns {
       display: grid;
       grid-template-columns: repeat(2, minmax(0, 1fr));
       gap: 1rem;
    }

---

<!-- _footer: ![](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png) © 2023 Quantuloop & GCQ-UFSC -->

$\phantom{\begin{matrix}
    \\\\\\\\\\\\
\end{matrix}}$

![w:170](static/gcq.svg)


![bg](static/title.svg)

---

# **Agenda**

* **Parte 1**
  * Panorama Atual da Computação Quântica
  * Características da Computação Quântica
* **Parte 2**
  * Programação Quântica na Prática

---

<!-- paginate: true -->


![bg](static/vantagem.svg)

---

<!-- _color: #F4F8FF -->
<!-- _paginate: false -->


![bg](static/IBM-Quantum-DevRoadmap2022_Dark.jpeg)

## Perspectiva $\phantom{............}$

$\phantom{\begin{matrix}
    \\\\\\
\end{matrix}}$

---

Precisamos de 
➕ Qubits e ➖ Ruido
--------------------

![w:900 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](static/rsa-shor.png)

---

<style scoped>
    section {
        font-size: 24pt;
    }
</style>

![bg left:50% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](static/forbes.png)

1. **Produtos Farmacêuticos**
2. Cibersegurança
3. Marketing e Publicidade
4. **Treinamento de IA**
5. **Serviços Financeiros**
6. **Logística**
7. Assistência Médica
8. Segurança Nacional
9. **Novos Materiais**
10. E-Sports/Jogos
11. Mercado Imobiliário
12. Petróleo e Gás

---

# **Características** da Computação Quântica

---

## $\phantom{.....}$ Superposição

![bg](static/super_plot.svg)

$\phantom{\begin{matrix}
    \\\\\\\\\\\\\
\end{matrix}}$

---

## Entrelaçamento

$${\color{#828b97}\sum_{x=0}^{2^n-1}\alpha_x}\ket{x}\ket{0} \xrightarrow{U_f} {\color{#828b97}\sum_{x=0}^{2^n-1}\alpha_x}\ket{x}\ket{f(x)}$$

## Medida

$${\color{#828b97}\sum_{x=0}^{2^n-1}\alpha_x}\ket{x}\ket{f(x)} \Rightarrow {\color{#828b97}\sum_{\color{#3d4a58} x'\,|\, f(x')=y}^{2^n-1}\alpha_{x'}}\ket{x'}\ket{y}$$

---

# Algoritmo Quântico 

$\phantom{\begin{matrix}
    \\\\\\\\\\\\\\\\
\end{matrix}}$

![bg fit](static/qa.svg)

---

# Não Clonagem

**Não** existe uma operação $U$ que satisfaça
$$\ket{\psi}\ket{\varphi}\xrightarrow{U}\ket{\psi}\ket{\psi}$$

# Decoerência 

Perda da Informação Quântica

---

![w:400](https://quantumket.org/_static/ket.svg)

# Plataforma da **Programação Quântica**

---

![bg 75%](https://quantumket.org/_images/runtime.png)

## Runtime

---

<div class="columns">
<div>

### Superposição

```py
from ket import *

qubits = quant(n)
H(qubits)
```

$$
\ket{0\cdots0} \xrightarrow{H^{\otimes n}} \sum_{k=0}^{2^n-1} \frac{1}{\sqrt{2^n}}\ket{k}
$$

</div>
<div>


### Entrelaçamento

```py
a, b = quant(2)
H(a)
with control(a):
    X(b)
```

$$\begin{matrix}
\ket{0_a}\ket{0_b} & \xrightarrow{H_a}         &  \frac{1}{\sqrt{2}}\left(\ket{0_a}+\ket{1_a}\right)\ket{0_b}\\
                   & \xrightarrow{\text{CNOT}} & \frac{1}{\sqrt{2}}\left(\ket{0_a0_b}+\ket{1_a1_b}\right)
\end{matrix}$$


</div>
</div>

---

## Medida

<div class="columns">
<div>


```pycon
>>> from ket import *
>>>
>>> qubits = quant(n)
>>> H(qubits)
>>> measure(qubits).value
14
```

$$
\sum_{k=0}^{2^n-1} \frac{1}{\sqrt{2^n}}\ket{k} \Rightarrow p(k) = \tfrac{1}{2^n}
$$

</div>
<div>



```pycon
>>> a, b = quant(2)
>>> H(a)
>>> with control(a):
...     X(b)
...
>>> ma = measure(a)
>>> mb = measure(b)
>>> assert ma.value == mb.value
```

$$\begin{matrix}
\frac{1}{\sqrt{2}}\left(\ket{0_a0_b}+\ket{1_a1_b}\right) & \Rightarrow     &  p(0_a0_b) = 0.5\\
                   & \Rightarrow     &  p(1_a1_b) = 0.5
\end{matrix}$$


</div>
</div>

---

# **Simulador Quântico** 

Testar **aplicações quânticas** em <br> um **computador clássico**

---

<div class="columns">
<div>

## Ket Bitwise Simulator


![h:200](https://gitlab.com/quantum-ket/quantum-ket.gitlab.io/-/raw/master/logos/kbw.svg)
Simulador Quântico CPU
Instalado com o Ket


</div>
<div>

## Similador QuBOX - UFSC

![h:200 drop-shadow:0,5px,10px,rgba(0,0,0,.4)](https://qubox.ufsc.br/_images/qubox.png)
Simulador Quântico GPU  <br> Acesso Remoto

</div>
</div>

---

![bg fit](static/kbw_algoritmo_quantico_de_estimacao_de_fase.svg)

<!-- _footer: Ket 0.5.3; Python 3.11.4; Linux 6.4.7; Intel Core i7-8565U -->

---

![bg fit](static/kbw_w_preparacao.svg)

<!-- _footer: Ket 0.5.3; Python 3.11.4; Linux 6.4.7; Intel Core i7-8565U -->

---

<!-- _backgroundImage: url('static/bg_br.svg') -->

![bg fit](static/kbw_vs_qbox_algoritmo_quantico_de_estimacao_de_fase.svg)

<!-- _footer: Ket 0.5.3; Python 3.11.4; Linux 6.4.7; Intel Core i7-8565U -->

---

# Programação Quântica na **Pratica**

![w:350](static/qr-code.svg)
https://bit.ly/ket-wcq2023
