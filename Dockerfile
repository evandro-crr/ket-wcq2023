FROM jupyter/base-notebook@sha256:31a02d408e757df3b12d9cdec6c37c6f073c67e343ff886fbf10fb64d5c01b34

COPY . /home/jovyan/

RUN python3 -m pip install --no-cache-dir \
    ket-lang==0.6.1 \
    qutip==4.7.2 \
    matplotlib==3.7.2
